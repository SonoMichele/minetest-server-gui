import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Console from "../views/Console";
import Files from "@/views/Files";
import Settings from "@/views/Settings";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/console',
    name: 'Console',
    component: Console
  },
  {
    path: '/files',
    name: 'Files',
    component: Files
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
