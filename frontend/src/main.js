import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

function configureEventSource() {
    const eventSource = new EventSource('http://127.0.0.1:5000/console/stream')
    eventSource.onmessage = function (event) {
        store.dispatch('consoleStream/outputStream', event.data)
    }
}

configureEventSource()

createApp(App).use(store).use(router).mount('#app')
