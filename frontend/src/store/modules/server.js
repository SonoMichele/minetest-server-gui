const axios = require('axios').default;
const API_URL = 'http://localhost:5000/console/'

export const serverModule = {
    namespaced: true,

    state: {
        serverStatus: '',
        serverSettings: ''
    },

    mutations: {
        SET_SERVER_STATUS(state, status) {
            state.serverStatus = status
        },

        SET_SERVER_SETTINGS(state, settings) {
            state.serverSettings = settings
        }
    },

    actions: {
        async startServer({ commit, dispatch }) {
            let response = await axios.get(API_URL + 'start')
            dispatch('notificationsModule/addNotification', {
                type: response.data.category,
                message: response.data.msg
            }, { root: true })
            if (response.status == "started" || response.status == "already_running") {
                commit('SET_SERVER_STATUS', 'running')
            } else {
                commit('SET_SERVER_STATUS', 'not_running')
            }
        },

        async stopServer({ commit, dispatch }) {
            let response = await axios.get(API_URL + 'stop')
            dispatch('notificationsModule/addNotification', {
                type: response.data.category,
                message: response.data.msg
            }, { root: true })
            if (response.status == "stopped" || response.status == "already_stopped") {
                commit('SET_SERVER_STATUS', 'not_running')
            } else {
                commit('SET_SERVER_STATUS', 'running')
            }
        },

        async getServerSettings({ commit }) {
            let response = await axios.get(API_URL + 'settings')
            // console.log(response.data[0].labelText)
            commit('SET_SERVER_SETTINGS', response.data)
            return response.data
        }
    },

    modules: {
    }
}
