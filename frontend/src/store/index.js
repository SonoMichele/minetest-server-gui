import { createStore } from 'vuex'
import { consoleModule } from "./modules/console";
import { serverModule } from "./modules/server";
import { notificationsModule } from "./modules/notifications";

export default createStore({
  state: {
    darkMode: true
  },
  mutations: {
    TOGGLE_DARK_MODE(state) {
      state.darkMode = !state.darkMode
    }
  },
  actions: {
    toggleDarkMode({ commit }) {
      commit('TOGGLE_DARK_MODE')
    }
  },
  modules: {
    consoleStream: consoleModule,
    serverAPI: serverModule,
    notificationsModule: notificationsModule
  }
})
